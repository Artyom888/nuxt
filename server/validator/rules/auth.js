const { body } = require('express-validator')

const loginRules = [
  body('email')
    .exists()
    .bail()
    .isEmail()
    .bail()
    .normalizeEmail(),
  body('password')
    .exists()
    .bail()
    .isLength({ min: 6, max: 16 })
    .bail()
    .trim()
    .escape()
]

const registerRules = [
  body('userName')
    .exists()
    .bail()
    .isLength({ min: 4 })
    .bail(),
  body('email')
    .exists()
    .bail()
    .isEmail()
    .bail()
    .normalizeEmail(),
  body('password')
    .exists()
    .bail()
    .isLength({ min: 6, max: 16 })
    .bail()
    .trim()
    .escape()
]

module.exports = { loginRules, registerRules }
