const router = require('express').Router()
const ReportsController = require('../controllers/ReportsController')
// Rules
router.route('/').get(ReportsController.get)

module.exports = router
