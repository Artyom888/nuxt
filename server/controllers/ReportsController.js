const db = require('../database/jsonDB')

const get = (req, res) => {
  const reports = db.get('reports').value()
  res.status(200).json(reports)
}

module.exports = { get }
