export const state = () => ({
  snackbar: {
    value: false,
    timeout: 5000,
    x: 'right',
    y: 'top',
    text: '',
    color: 'red'
  }
})
export const mutations = {
  snackbarOpen(state, payload) {
    state.snackbar.value = true
    Object.keys(payload).forEach((key) => {
      state.snackbar[key] = payload[key]
    })
  },
  snackbarClose(state) {
    state.snackbar.value = false
  }
}
