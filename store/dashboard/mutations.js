export default {
  mutateLoading(state, payload) {
    state.loading = payload
  },
  getReportsSuccess(state, payload) {
    state.reports = payload
  }
}
