import { REPORTS } from '@/links'

export default {
  async getReports({ commit }) {
    commit('mutateLoading', true)
    try {
      const reports = await this.$axios.$get(REPORTS)
      commit('getReportsSuccess', reports)
    } catch (e) {
      commit('snackbarOpen', { text: e.response.data }, { root: true })
    } finally {
      commit('mutateLoading', false)
    }
  }
}
