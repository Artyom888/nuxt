import tokenParser from '~/helpers/helperCollections'

export default () => ({
  user: tokenParser('user')
})
